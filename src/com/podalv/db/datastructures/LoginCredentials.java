package com.podalv.db.datastructures;

/** A simple user / password combination
 *
 * @author podalv
 *
 */
public class LoginCredentials {

  private final String userName;
  private final String password;

  public LoginCredentials(final String userName, final String password) {
    this.userName = userName;
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public String getUserName() {
    return userName;
  }
}
