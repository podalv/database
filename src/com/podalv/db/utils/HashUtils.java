package com.podalv.db.utils;

import java.security.MessageDigest;

/** Allows calculating and comparing passwords using SHA-512 hash
 *
 * @author podalv
 *
 */
public class HashUtils {

  public static final String HASH_METHOD = "SHA-512";

  private static String hashBytesToString(final byte[] bytes) {
    final StringBuilder result = new StringBuilder();
    for (int i = 0; i < bytes.length; i++) {
      result.append(Integer.toHexString(0xFF & bytes[i]));
    }
    return result.toString();
  }

  public static String calculateHash(final String password) {
    try {
      final MessageDigest mda = MessageDigest.getInstance(HASH_METHOD);
      return hashBytesToString(mda.digest(password.getBytes()));
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static boolean comparePasswordWithHash(final String password, final String hash) {
    return calculateHash(password).equals(hash);
  }
}
