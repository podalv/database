package com.podalv.db.datastructures;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

/** Encapsulates basic connection settings that allow establishing a database connection
 *  - URL
 *  - login
 *  - password
 *
 * @author podalv
 *
 */
public class ConnectionSettings {

  public static final String     LOGIN_KEY    = "LOGIN";
  public static final String     PASSWORD_KEY = "PASSWORD";
  public static final String     PATH_KEY     = "PATH";
  private final String           url;
  private final LoginCredentials login;
  private Connection             connection   = null;

  /** For test purposes only. Created connection settings that already contain the connection that will be reused in the Database
   *  instead of fetching a new connetion
   *
   * @return
   */
  public static ConnectionSettings createFromConnection(final Connection connection) {
    return new ConnectionSettings(connection);
  }

  public static ConnectionSettings createFromUrlNamePassword(final String url, final String name, final String password) {
    return new ConnectionSettings(url, new LoginCredentials(name, password));
  }

  public static ConnectionSettings createFromFile(final File file) throws FileNotFoundException, IOException {
    final Properties prop = new Properties();
    prop.load(new FileInputStream(file));
    return new ConnectionSettings(prop.getProperty(PATH_KEY), new LoginCredentials(prop.getProperty(LOGIN_KEY), prop.getProperty(PASSWORD_KEY)));
  }

  private ConnectionSettings(final Connection connection) {
    this.url = null;
    this.login = null;
    this.connection = connection;
  }

  public boolean hasConnection() {
    return connection != null;
  }

  public Connection getConnection() {
    return connection;
  }

  public ConnectionSettings(final String url, final LoginCredentials login) {
    this.login = login;
    this.url = url;
  }

  public String getUrl() {
    return url;
  }

  public String getUserName() {
    return login.getUserName();
  }

  public String getPassword() {
    return login.getPassword();
  }

}
