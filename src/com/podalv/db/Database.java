package com.podalv.db;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.podalv.db.datastructures.ConnectionSettings;

/** Allows opening a database connection to a MySql database
 *
 * @author podalv
 *
 */
public class Database implements Closeable {

  public static boolean            LOGGING_ENABLED = true;
  private static Database          instance        = null;
  private Connection               connection;
  private final ConnectionSettings settings;

  static {
    try {
      Class.forName(com.mysql.jdbc.Driver.class.getCanonicalName());
    }
    catch (final SecurityException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (final ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static Database getInstance() {
    return instance;
  }

  public static Database create(final ConnectionSettings settings) throws SQLException {
    return new Database(settings, true);
  }

  public static Database createInstance(final ConnectionSettings settings) throws SQLException {
    return new Database(settings, false);
  }

  public Connection getConnection() throws SQLException {
    try {
      if (connection.isValid(5) && !connection.isClosed()) {
        return connection;
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    connection = DriverManager.getConnection(settings.getUrl(), settings.getUserName(), settings.getPassword());
    return connection;
  }

  public static String escapeSql(String str) {
    str = str.replaceAll("'", "''");
    str = str.replaceAll("\"", "\"\"");
    str = str.replaceAll("\\\\", "");
    return str;
  }

  private Database(final ConnectionSettings settings, final boolean cacheInstance) throws SQLException {
    this.settings = settings;
    if (instance != null && cacheInstance) {
      throw new UnsupportedOperationException("There can be only one instance of the Database");
    }
    if (settings.hasConnection()) {
      connection = settings.getConnection();
    }
    else {
      connection = DriverManager.getConnection(settings.getUrl(), settings.getUserName(), settings.getPassword());
    }
    if (cacheInstance) {
      instance = this;
    }
  }

  public ResultSet query(final String sqlQuery) throws SQLException {
    if (LOGGING_ENABLED) {
      System.out.println("QUERY = '" + sqlQuery + "'");
    }
    final Statement stat = getConnection().prepareStatement(sqlQuery);
    return stat.executeQuery(sqlQuery);
  }

  public ResultSet queryStreaming(final String sqlQuery) throws SQLException {
    if (LOGGING_ENABLED) {
      System.out.println("QUERY = '" + sqlQuery + "'");
    }
    final Statement stat = getConnection().prepareStatement(sqlQuery, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    stat.setFetchSize(Integer.MIN_VALUE);
    return stat.executeQuery(sqlQuery);
  }

  public static void closeQuery(final ResultSet set) {
    try {
      set.getStatement().close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }

  public static void closeStatement(final Statement statement) {
    if (statement != null) {
      try {
        statement.close();
      }
      catch (final SQLException e) {
        e.printStackTrace();
      }
    }
  }

  public void update(final String sqlCommand) {
    if (LOGGING_ENABLED) {
      System.out.println("COMMAND = '" + sqlCommand + "'");
    }
    Statement statement = null;
    try {
      statement = getConnection().createStatement();
      statement.executeUpdate(sqlCommand);
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    finally {
      closeStatement(statement);
    }
  }

  public boolean stringQueryEquals(final String sqlQuery, final String columnName, final String expectedResult) {
    try {
      final ResultSet set = query(sqlQuery);
      if (set.first()) {
        try {
          return (set.getString(columnName).equals(expectedResult) && !set.next());
        }
        finally {
          closeQuery(set);
        }
      }
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  @Override
  public void close() throws IOException {
    try {
      connection.close();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
