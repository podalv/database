package com.podalv.db.test;

import java.sql.ResultSet;

import org.junit.Assert;
import org.junit.Test;

public class MockResultSetTest {

  @Test
  public void columnIds() throws Exception {
    final MockDataTable table = new MockDataTable("first", "second", "third", "fourth");
    table.addRow("1", "0.5", "String", "false");
    table.addRow("2", "0.7", "Else", "true");
    final ResultSet set = new MockResultSet(table);
    Assert.assertTrue(set.next());
    Assert.assertEquals(1, set.getInt(1));
    Assert.assertEquals(0.5, set.getDouble(2), 0.001);
    Assert.assertEquals(0.5, set.getFloat(2), 0.001);
    Assert.assertEquals("String", set.getString(3));
    Assert.assertFalse(set.getBoolean(4));
    Assert.assertTrue(set.next());
    Assert.assertEquals(2, set.getInt(1));
    Assert.assertEquals(0.7, set.getDouble(2), 0.001);
    Assert.assertEquals(0.7, set.getFloat(2), 0.001);
    Assert.assertEquals("Else", set.getString(3));
    Assert.assertTrue(set.getBoolean(4));
    Assert.assertFalse(set.next());
    set.close();
  }

  @Test
  public void columnNames() throws Exception {
    final MockDataTable table = new MockDataTable("first", "second", "third", "fourth");
    table.addRow("1", "0.5", "String", "false");
    table.addRow("2", "0.7", "Else", "true");
    final ResultSet set = new MockResultSet(table);
    Assert.assertTrue(set.next());
    Assert.assertEquals(1, set.getInt("first"));
    Assert.assertEquals(0.5, set.getDouble("second"), 0.001);
    Assert.assertEquals(0.5, set.getFloat("second"), 0.001);
    Assert.assertEquals("String", set.getString("third"));
    Assert.assertFalse(set.getBoolean("fourth"));
    Assert.assertTrue(set.next());
    Assert.assertEquals(2, set.getInt("first"));
    Assert.assertEquals(0.7, set.getDouble("second"), 0.001);
    Assert.assertEquals(0.7, set.getFloat("second"), 0.001);
    Assert.assertEquals("Else", set.getString("third"));
    Assert.assertTrue(set.getBoolean("fourth"));
    Assert.assertFalse(set.next());
    set.close();
  }

}
