package com.podalv.db.test;

import java.util.ArrayList;
import java.util.Iterator;

public class MockDataTable {

  private final String[] columns;
  ArrayList<String[]>    rows = new ArrayList<String[]>();

  public MockDataTable(final String ... columns) {
    this.columns = columns;
  }

  public MockDataTable addRow(final String ... row) {
    final String[] arrayToAdd = new String[row.length + 1];
    for (int x = 0; x < row.length; x++) {
      arrayToAdd[x + 1] = row[x];
    }
    rows.add(arrayToAdd);
    return this;
  }

  public int getColumnId(final String column) {
    for (int x = 0; x < columns.length; x++) {
      if (column.equals(columns[x])) {
        return x + 1;
      }
    }
    return -1;
  }

  public Iterator<String[]> iterator() {
    return rows.iterator();
  }
}
